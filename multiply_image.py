#!/usr/bin/python
import shutil
import os
import sys
import argparse

def copy_image(image,destination,noc):

  for n in range(int(noc)):
      shutil.copy(image, destination+str(n).zfill(6)+".png")
  print("Copying is succesfully finished!\n");

if __name__ == "__main__":
  parser = argparse.ArgumentParser(
      description='makes a copy of given image named in ascending order')
  parser.add_argument('-i', metavar='INPUT_IMAGE', required=True, help='input image')
  parser.add_argument('-d', metavar='DESTINATION_PATH', required=True, help='destination path')
  parser.add_argument('-n', metavar='NO_OF_COPIES', required=True, help='number of copies')
  args = parser.parse_args()

  try:
    copy_image(args.i,args.d,args.n)
  except Exception, e:
    import traceback
    traceback.print_exc()
